Cordova plugin wrapper for Time
===============================
This wrapper packages and builds the platform-specific modules
offering Time support for a Cordova-based application.

Documentation
-------------
To add timer/real-time clock support to a Cordova-based application,
issue the following command in the application folder:
```
cordova plugin add https://bitbucket.org/4s/cdvw-time.git
```

Dependency
----------
There does not exist an explicit dependency on the cordova adaptor(https://bitbucket.org/4s/cordova-adaptor/)
but any use of this project requires the cordova adapter to be 
installed also.

During installation of this project two files are copied into the
cordova infrastructure:

- module-name.txt
- src/android/build-extras.gradle

The gradle file will pull the time module (https://bitbucket.org/4s/time-android-module)
and module-name file is used by the cordova-adaptor to start the
module with that name from the time module.

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0